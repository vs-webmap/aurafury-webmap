fl="data/mw/in.list"
mvrt="data/mw/out.vrt"
find data/mw/world -name "*.png" > $fl
we="worldExtent.js"

rm $mvrt
gdalbuildvrt -hidenodata -srcnodata "0 0 0" -input_file_list $fl $mvrt

rm -rf data/mw/rasters/
gdal2tiles.py -p raster -e -r lanczos -z 0-9 -s epsg:3857 --processes=8 -x -w all --xyz $mvrt data/mw/rasters/


echo "Extracting the new extent..."
echo "var vsWorldGrid = new ol.tilegrid.TileGrid({" > $we
grep -A3 extent data/mw/rasters/openlayers.html | sed -e 's/^[ \t]*//' >> $we
truncate -s-1 $we
echo -e " });" >> $we
# echo -e ",\ntilePixelRatio:2 })" >> $we
echo "Done!"