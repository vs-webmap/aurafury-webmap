var vsWorldGrid = new ol.tilegrid.TileGrid({
extent: [-8448.5,-5887.5,6655.5,9216.5],
origin: [-8448.5,9216.5],
resolutions: [64,32,16,8,4,2,1],
tileSize: [256, 256] });
